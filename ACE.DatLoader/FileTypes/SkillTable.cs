using System;
using System.Collections.Generic;
using System.IO;

using ACE.DatLoader.Entity;
using UtilityBelt.Common.Enums;

namespace ACE.DatLoader.FileTypes
{
    [DatFileType(DatFileType.SkillTable)]
    public class SkillTable : FileType
    {
        internal const uint FILE_ID = 0x0E000004;

        // Key is the SkillId
        public Dictionary<uint, SkillBase> SkillBaseHash = new Dictionary<uint, SkillBase>();

        public override void Unpack(BinaryReader reader)
        {
            Id = reader.ReadUInt32();
            SkillBaseHash.UnpackPackedHashTable(reader);
        }

        public void AddRetiredSkills()
        {
            SkillBaseHash.Add((int)SkillId.Axe, new SkillBase(new SkillFormula(AttributeId.Strength, AttributeId.Coordination, 3)));
            SkillBaseHash.Add((int)SkillId.Bow, new SkillBase(new SkillFormula(AttributeId.Coordination, AttributeId.Undef, 2)));
            SkillBaseHash.Add((int)SkillId.Crossbow, new SkillBase(new SkillFormula(AttributeId.Coordination, AttributeId.Undef, 2)));
            SkillBaseHash.Add((int)SkillId.Dagger, new SkillBase(new SkillFormula(AttributeId.Quickness, AttributeId.Coordination, 3)));
            SkillBaseHash.Add((int)SkillId.Mace, new SkillBase(new SkillFormula(AttributeId.Strength, AttributeId.Coordination, 3)));
            SkillBaseHash.Add((int)SkillId.Spear, new SkillBase(new SkillFormula(AttributeId.Strength, AttributeId.Coordination, 3)));
            SkillBaseHash.Add((int)SkillId.Staff, new SkillBase(new SkillFormula(AttributeId.Strength, AttributeId.Coordination, 3)));
            SkillBaseHash.Add((int)SkillId.Sword, new SkillBase(new SkillFormula(AttributeId.Strength, AttributeId.Coordination, 3)));
            SkillBaseHash.Add((int)SkillId.ThrownWeapons, new SkillBase(new SkillFormula(AttributeId.Coordination, AttributeId.Undef, 2)));
            SkillBaseHash.Add((int)SkillId.UnarmedCombat, new SkillBase(new SkillFormula(AttributeId.Strength, AttributeId.Coordination, 3)));
        }
    }
}
