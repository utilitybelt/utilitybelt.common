using System.IO;

//using log4net;

namespace ACE.DatLoader
{
    public static class DatManager
    {
        //private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static string datFile;

        private static int count;

        // End of retail Iteration versions.
        //private static int ITERATION_CELL = 982;
        //private static int ITERATION_PORTAL = 2072;
        //private static int ITERATION_HIRES = 497;
        //private static int ITERATION_LANGUAGE = 994;
        public static CellDatDatabase CellDat { get; internal set; }

        public static PortalDatDatabase PortalDat { get; internal set; }
        public static DatDatabase HighResDat { get; private set; }
        public static LanguageDatDatabase LanguageDat { get; private set; }

        public static void Initialize(string datFileDirectory, bool keepOpen = false, bool loadCell = true)
        {
            var datDir = Path.GetFullPath(datFileDirectory);

            if (loadCell)
            {

                datFile = Path.Combine(datDir, "client_cell_1.dat");
                CellDat = new CellDatDatabase(datFile, keepOpen);
                count = CellDat.AllFiles.Count;
            }


            datFile = Path.Combine(datDir, "client_portal.dat");
            PortalDat = new PortalDatDatabase(datFile, keepOpen);
            PortalDat.SkillTable.AddRetiredSkills();
            count = PortalDat.AllFiles.Count;

            // Load the client_highres.dat file. This is not required for ACE operation, so no exception needs to be generated.
            datFile = Path.Combine(datDir, "client_highres.dat");
            if (File.Exists(datFile))
            {
                HighResDat = new DatDatabase(datFile, keepOpen);
                count = HighResDat.AllFiles.Count;
                //log.Info($"Successfully opened {datFile} file, containing {count} records, iteration {HighResDat.Iteration}");
                //if (HighResDat.Iteration != ITERATION_HIRES)
                //    log.Warn($"{datFile} iteration does not match expected end-of-retail version of {ITERATION_HIRES}.");
            }


                datFile = Path.Combine(datDir, "client_local_English.dat");
                LanguageDat = new LanguageDatDatabase(datFile, keepOpen);
                count = LanguageDat.AllFiles.Count;
            }
    }
}
