using System.IO;
using UtilityBelt.Common.Enums;

namespace ACE.DatLoader.Entity
{
    public class CellPortal : IUnpackable
    {
        public CellPortalFlags Flags { get; private set; }
        public ushort PolygonId { get; private set; }
        public ushort OtherCellId { get; private set; }
        public ushort OtherPortalId { get; private set; }

        public bool ExactMatch => (Flags & CellPortalFlags.ExactMatch) != 0;
        public bool PortalSide => (Flags & CellPortalFlags.PortalSide) == 0;

        public void Unpack(BinaryReader reader)
        {
            Flags           = (CellPortalFlags)reader.ReadUInt16();
            PolygonId       = reader.ReadUInt16();
            OtherCellId     = reader.ReadUInt16();
            OtherPortalId   = reader.ReadUInt16();
        }
    }
}
