using System.IO;
using UtilityBelt.Common.Messages.Types;

namespace ACE.DatLoader.Entity
{
    public class Plane : IUnpackable
    {
        public Vector3 N { get; private set; }
        public float D { get; private set; }

        public void Unpack(BinaryReader reader)
        {
            N = reader.ReadVector3();
            D = reader.ReadSingle();
        }
    }
}
