using System;

using UtilityBelt.Common.Enums;

namespace ACE.Entity
{
    public class AttackFrameParams : IEquatable<AttackFrameParams>
    {
        public uint MotionTableId;
        public MotionDatStance Stance;
        public MotionDatCommand Motion;

        public AttackFrameParams(uint motionTableId, MotionDatStance stance, MotionDatCommand motion)
        {
            MotionTableId = motionTableId;
            Stance = stance;
            Motion = motion;
        }

        public bool Equals(AttackFrameParams attackFrameParams)
        {
            return MotionTableId == attackFrameParams.MotionTableId &&
                Stance == attackFrameParams.Stance &&
                Motion == attackFrameParams.Motion;
        }

        public override int GetHashCode()
        {
            int hash = 0;

            hash = (hash * 397) ^ MotionTableId.GetHashCode();
            hash = (hash * 397) ^ Stance.GetHashCode();
            hash = (hash * 397) ^ Motion.GetHashCode();

            return hash;
        }
    }
}
