using System;
using System.Collections.Generic;

using UtilityBelt.Common.Enums;

namespace ACE.Entity.Models
{
    /// <summary>
    /// Only populated collections and dictionaries are initialized.
    /// We do this to conserve memory in ACE.Server
    /// Be sure to check for null first.
    /// </summary>
    public class Biota : IWeenie
    {
        public uint Id { get; set; }
        public uint WeenieClassId { get; set; }
        public WeenieType WeenieType { get; set; }

        public IDictionary<BoolId, bool> PropertiesBool { get; set; }
        public IDictionary<DataId, uint> PropertiesDID { get; set; }
        public IDictionary<FloatId, double> PropertiesFloat { get; set; }
        public IDictionary<InstanceId, uint> PropertiesIID { get; set; }
        public IDictionary<IntId, int> PropertiesInt { get; set; }
        public IDictionary<Int64Id, long> PropertiesInt64 { get; set; }
        public IDictionary<StringId, string> PropertiesString { get; set; }

        public IDictionary<PositionPropertyID, PropertiesPosition> PropertiesPosition { get; set; }

        public IDictionary<int, float /* probability */> PropertiesSpellBook { get; set; }

        public IList<PropertiesAnimPart> PropertiesAnimPart { get; set; }
        public IList<PropertiesPalette> PropertiesPalette { get; set; }
        public IList<PropertiesTextureMap> PropertiesTextureMap { get; set; }

        // Properties for all world objects that typically aren't modified over the original weenie
        public ICollection<PropertiesCreateList> PropertiesCreateList { get; set; }
        public ICollection<PropertiesEmote> PropertiesEmote { get; set; }
        public HashSet<int> PropertiesEventFilter { get; set; }
        public IList<PropertiesGenerator> PropertiesGenerator { get; set; }

        // Properties for creatures
        public IDictionary<AttributeId, PropertiesAttribute> PropertiesAttribute { get; set; }
        public IDictionary<VitalId, PropertiesAttribute2nd> PropertiesAttribute2nd { get; set; }
        public IDictionary<CombatBodyPart, PropertiesBodyPart> PropertiesBodyPart { get; set; }
        public IDictionary<SkillId, PropertiesSkill> PropertiesSkill { get; set; }

        // Properties for books
        public PropertiesBook PropertiesBook { get; set; }
        public IList<PropertiesBookPageData> PropertiesBookPageData { get; set; }

        // Biota additions over Weenie
        public IDictionary<uint /* Character ID */, PropertiesAllegiance> PropertiesAllegiance { get; set; }
        public ICollection<PropertiesEnchantmentRegistry> PropertiesEnchantmentRegistry { get; set; }
        public IDictionary<uint /* Player GUID */, bool /* Storage */> HousePermissions { get; set; }
    }
}
