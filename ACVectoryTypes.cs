﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityBelt.Common.Lib;

namespace UtilityBelt.Common.Messages.Types {
    /// <summary>
    /// List which is packable for network
    /// </summary>
    public class PackableList<T> {
        /// <summary>
        /// Number of items in the list
        /// </summary>
        public int Count;

        /// <summary>
        /// Holds the actual item data
        /// </summary>
        public List<T> Items { get; set; } = new List<T>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Count = BinaryHelpers.ReadInt32(buffer);
            for (var i = 0; i < Count; i++) {
                Items.Add(BinaryHelpers.ReadItem<T>(buffer));
            }
        }
    }

    /// <summary>
    /// HashTable which is packable for network
    /// </summary>
    public class PackableHashTable<T, U> : IACDataType {
        /// <summary>
        /// number of items in the table
        /// </summary>
        public short Count;

        /// <summary>
        /// max size of the table
        /// </summary>
        public short TableSize;

        /// <summary>
        /// Holds the actual table data
        /// </summary>
        public Dictionary<T, U> Table = new Dictionary<T, U>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Count = BinaryHelpers.ReadInt16(buffer);
            TableSize = BinaryHelpers.ReadInt16(buffer);
            for (var i = 0; i < Count; i++) {
                T key = BinaryHelpers.ReadItem<T>(buffer);
                U value = BinaryHelpers.ReadItem<U>(buffer);
                Table.Add(key, value);
            }
        }
    }

    /// <summary>
    /// List which is packable for network
    /// </summary>
    public class PList<T> : IACDataType {
        /// <summary>
        /// Number of items in the list
        /// </summary>
        public uint Count;

        /// <summary>
        /// Holds the actual list data
        /// </summary>
        public List<T> Items { get; set; } = new List<T>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Count; i++) {
                Items.Add(BinaryHelpers.ReadItem<T>(buffer));
            }
        }
    }

    /// <summary>
    /// HashTable which is packable for network
    /// </summary>
    public class PHashTable<T, U> : IACDataType {
        /// <summary>
        /// packedSize
        /// </summary>
        public uint PackedSize;

        /// <summary>
        /// Derived from packedSize. 
        /// </summary>
        public uint Buckets { get => (uint)1 << ((int)PackedSize >> 24); }

        /// <summary>
        /// Derived from packedSize. 
        /// </summary>
        public uint Count { get => (PackedSize & 0xFFFFFF); }

        /// <summary>
        /// Holds the actual table data
        /// </summary>
        public Dictionary<T, U> Table { get; set; } = new Dictionary<T, U>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            PackedSize = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Count; i++) {
                T key = BinaryHelpers.ReadItem<T>(buffer);
                U value = BinaryHelpers.ReadItem<U>(buffer);

                if (!Table.ContainsKey(key)) {
                    Table.Add(key, value);
                }
            }
        }
    }

    /// <summary>
    /// List which is packable for network
    /// </summary>
    public class PSmartArray<T> : IACDataType {
        /// <summary>
        /// Number of items in the list
        /// </summary>
        public uint Count;

        /// <summary>
        /// Holds the actual array data
        /// </summary>
        public List<T> Items { get; set; } = new List<T>();

        /// <summary>
        /// Reads instance data from a binary reader
        /// </summary>
        public void ReadFromBuffer(BinaryReader buffer) {
            Count = BinaryHelpers.ReadUInt32(buffer);
            for (var i = 0; i < Count; i++) {
                Items.Add(BinaryHelpers.ReadItem<T>(buffer));
            }
        }
    }
}
