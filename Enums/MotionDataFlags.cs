using System;

namespace UtilityBelt.Common.Enums {
    [Flags]
    public enum MotionDataFlags: byte {
        HasVelocity = 0x1,
        HasOmega    = 0x2
    }
}
