namespace UtilityBelt.Common.Enums {
    public enum EmitterType {
        Unknown = 0,
        BirthratePerSec = 1,
        BirthratePerMeter = 2,
    }
}
