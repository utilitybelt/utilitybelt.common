﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace UtilityBelt.Common.Lib {
    public static class Extensions {
        public static void InvokeSafely<T>(this EventHandler<T> eventHandler, object sender, T eventArgs) where T : EventArgs {
            File.WriteAllText(@"C:\Turbine\Asheron's Call\ubservice.exceptions.txt", $"InvokeSafely");
            if (eventHandler != null) eventHandler(sender, eventArgs);
        }
    }
}
