﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Common.Enums;
using UtilityBelt.Common.Messages.Types;

namespace UtilityBelt.Common.Lib {
    public static class ServerSpellData {
        private static bool _didLoad = false;
        public static Dictionary<uint, SpellInfo> SpellInfo = new Dictionary<uint, SpellInfo>();
        private static void LoadSpellData() {
            if (_didLoad)
                return;
            _didLoad = true;
            using (Stream manifestResourceStream = typeof(ServerSpellData).Assembly.GetManifestResourceStream("UtilityBelt.Common.Resources.serverspelldata.json")) {
                var bytes = new byte[manifestResourceStream.Length];
                manifestResourceStream.Read(bytes, 0, bytes.Length);
                string json = Encoding.UTF8.GetString(bytes, 0, (int)manifestResourceStream.Length);
                JsonConvert.PopulateObject(json, SpellInfo);
            }
        }

        public static bool TryGetSpell(uint spellId, out SpellInfo spellInfo) {
            if (!_didLoad)
                LoadSpellData();

            return SpellInfo.TryGetValue(spellId, out spellInfo);
        }
    }

    public class SpellInfo {
        public uint Id;

        /// <summary>
        /// The stat modifier type, usually EnchantmentTypeFlags
        /// </summary>
        public EnchantmentFlags StatModType;

        /// <summary>
        /// The stat modifier key, used for lookup in the enchantment registry
        /// </summary>
        public int StatModKey;

        /// <summary>
        /// The amount to modify a stat
        /// </summary>
        public float StatModVal;

        /// <summary>
        /// The attribute this StatMod is for, if any.
        /// </summary>
        public AttributeId StatModAttribute => (StatModType & EnchantmentFlags.Attribute) != 0 ? (AttributeId)StatModKey : AttributeId.Undef;

        /// <summary>
        /// The vital this StatMod is for, if any.
        /// </summary>
        public UtilityBelt.Common.Enums.Vital StatModVital => (StatModType & EnchantmentFlags.Attribute2nd) != 0 ? (UtilityBelt.Common.Enums.Vital)StatModKey : UtilityBelt.Common.Enums.Vital.Undef;

        /// <summary>
        /// The skill this StatMod is for, if any.
        /// </summary>
        public SkillId StatModSkill => (StatModType & EnchantmentFlags.Skill) != 0 ? (SkillId)StatModKey : SkillId.Undef;

        /// <summary>
        /// The int property this StatMod is for, if any.
        /// </summary>
        public IntId StatModIntProp => (StatModType & EnchantmentFlags.Int) != 0 ? (IntId)StatModKey : IntId.Undef;

        /// <summary>
        /// The float property this StatMod is for, if any.
        /// </summary>
        public FloatId StatModFloatProp => (StatModType & EnchantmentFlags.Float) != 0 ? (FloatId)StatModKey : FloatId.Undef;

        /// <summary>
        /// The damage type for this spell
        /// </summary>
        public DamageType DamageType;

        /// <summary>
        /// The base amount of damage for this spell
        /// </summary>
        public int BaseIntensity;

        /// <summary>
        /// The maximum additional daamage for this spell
        /// </summary>
        public int Variance;

        /// <summary>
        /// The weenie class ID associated for this spell, ie. the projectile weenie class id
        /// </summary>
        public uint WeenieClassId;

        /// <summary>
        /// The total # of projectiles launched for this spell
        /// </summary>
        public int NumProjectiles;

        /// <summary>
        /// The maximum # of additional projectiles possibly launched
        /// </summary>
        public int NumProjectilesVariance;

        /// <summary>
        /// The total angle for multi-projectile spells,
        /// ie. 90 degrees for 3-5 projectiles, or 360 degrees for ring spells
        /// </summary>
        public float SpreadAngle;

        /// <summary>
        /// The vertical angle to launch this spell
        /// </summary>
        public float VerticalAngle;

        /// <summary>
        /// The default angle to launch this spell
        /// (relative to player, or global?)
        /// </summary>
        public float DefaultLaunchAngle;

        /// <summary>
        /// If this is on then projectile spells won't lead a target.
        /// Arc spells have this set to true.
        /// </summary>
        public bool NonTracking;

        /// <summary>
        /// The offset to apply to the spawn position
        /// </summary>
        public Vector3 CreateOffset = new Vector3();

        /// <summary>
        /// The minimum amount of padding to ensure for the spell to spawn
        /// </summary>
        public Vector3 Padding = new Vector3();

        /// <summary>
        /// The dimensions of the origin, used for Volley spells?
        /// </summary>
        public Vector3 Dims = new Vector3();

        /// <summary>
        /// The maximum variation for spawn position
        /// </summary>
        public Vector3 Peturbation = new Vector3();

        /// <summary>
        /// The imbued effect for this spell
        /// </summary>
        public uint ImbuedEffect;

        /// <summary>
        /// The creature class for a slayer spell
        /// </summary>
        public CreatureType SlayerCreatureType;

        /// <summary>
        /// The amount of additional damage for a slayer spell
        /// </summary>
        public float SlayerDamageBonus;

        /// <summary>
        /// The critical chance frequency for this spell
        /// </summary>
        public double CritFrequency;

        /// <summary>
        /// The critical damage multiplier for this spell
        /// </summary>
        public double CritMultiplier = 1;

        /// <summary>
        /// If TRUE, ignores magic resistance
        /// </summary>
        public bool IgnoreMagicResist;

        /// <summary>
        /// The elemental damage multiplier for this spell
        /// </summary>
        public double ElementalModifier = 1;

        /// <summary>
        /// The amount of source vital to drain for a life spell
        /// </summary>
        public float DrainPercentage;

        /// <summary>
        /// The percentage of DrainPercentage to damage a target for life projectiles
        /// </summary>
        public float DamageRatio = 1.0f;

        /// <summary>
        /// DamageType used by LifeMagic spells that specifies Health, Mana, or Stamina for the Boost type spells
        /// </summary>
        public DamageType VitalDamageType;

        /// <summary>
        /// The minimum amount of vital boost from a life spell
        /// </summary>
        public int Boost;

        /// <summary>
        /// Boost + BoostVariance = the maximum amount of vital boost from a life spell
        /// </summary>
        public int BoostVariance;

        /// <summary>
        /// The source vital for a life spell
        /// </summary>
        public Vital Source;

        /// <summary>
        /// The destination vital for a life spell
        /// </summary>
        public Vital Destination;

        /// <summary>
        /// The propotion of source vital to transfer to destination vital
        /// </summary>
        public float Proportion = 1.0f;

        /// <summary>
        /// The percent of source vital loss for a life magic transfer spell
        /// </summary>
        public float LossPercent = 0.0f;

        /// <summary>
        /// A static amount of source vital loss for a life magic transfer spell?
        /// Unused / unknown?
        /// </summary>
        public int SourceLoss = 0;

        /// <summary>
        /// The maximum amount of vital transferred by a life magic spell
        /// </summary>
        public int TransferCap;

        /// <summary>
        /// The maximum destination vital boost for a life magic transfer spell?
        /// Unused / unknown?
        /// </summary>
        public int MaxBoostAllowed;

        /// <summary>
        /// Indicates the source and destination for life magic transfer spells
        /// </summary>
        public TransferFlags TransferFlags;

        /// <summary>
        /// Unknown index?
        /// </summary>
        public int Index;

        /// <summary>
        /// For SpellType.PortalSummon spells, Link is set to either 1 for LinkedPortalOneDID or 2 for LinkedPortalTwoDID
        /// </summary>
        public int Link;

        /// <summary>
        /// The destination landcell for a spell
        /// </summary>
        public uint DestinationLandcell;

        /// <summary>
        /// The destination origin for a spell
        /// </summary>
        public Vector3 DestinationOrigin = new Vector3();

        /// <summary>
        /// The destination orientation for a spell
        /// </summary>
        public Quaternion DestinationOrientation = new Quaternion();

        /// <summary>
        /// The minimum spell power to dispel (unused?)
        /// </summary>
        public int MinPower;

        /// <summary>
        /// The maximum spell power to dispel
        /// </summary>
        public int MaxPower;

        /// <summary>
        /// Possible RNG for spell power to dispel (unused?)
        /// </summary>
        public float PowerVariance;

        /// <summary>
        /// The magic school to dispel, or undefined for all schools
        /// </summary>
        public MagicSchool DispelSchool;

        /// <summary>
        /// The type of spells to dispel
        /// 0 = all spells
        /// 1 = positive
        /// 2 = negative
        /// </summary>
        public DispelType Align;

        /// <summary>
        /// The maximum # of spells to dispel
        /// </summary>
        public int Number;

        /// <summary>
        /// Number * NumberVariance = the minimum # of spells to dispel
        /// </summary>
        public float NumberVariance;

        /// <summary>
        /// Damage over Time Duration?
        /// </summary>
        public float DotDuration;
    }

}
