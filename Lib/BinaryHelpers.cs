﻿using System;
using System.IO;
using System.Linq;
using UtilityBelt.Common.Messages.Types;

namespace UtilityBelt.Common.Lib {
    /// <summary>
    /// A collection of helper methods for reading AC network message binary streams
    /// </summary>
    internal static class BinaryHelpers {
        public static bool Debug = false;

        /// <summary>
        /// Read an item of type T from the buffer and return its value
        /// </summary>
        /// <typeparam name="T">type of item to read</typeparam>
        /// <param name="buffer">buffer to read from</param>
        /// <returns></returns>
        internal static T ReadItem<T>(BinaryReader buffer) {
            if (typeof(T).GetInterfaces().Contains(typeof(IACDataType))) {
                var item = Activator.CreateInstance<T>();
                (item as IACDataType).ReadFromBuffer(buffer);
                return item;
            }
            else {
                var type = typeof(T).IsEnum ? Enum.GetUnderlyingType(typeof(T)) : typeof(T);

                if (type == typeof(ushort))
                    return (T)(object)ReadUInt16(buffer);
                else if (type == typeof(short))
                    return (T)(object)ReadInt16(buffer);
                else if (type == typeof(uint))
                    return (T)(object)ReadUInt32(buffer);
                else if (type == typeof(int))
                    return (T)(object)ReadInt32(buffer);
                else if (type == typeof(ulong))
                    return (T)(object)ReadUInt64(buffer);
                else if (type == typeof(long))
                    return (T)(object)ReadInt64(buffer);
                else if (type == typeof(float))
                    return (T)(object)ReadSingle(buffer);
                else if (type == typeof(double))
                    return (T)(object)ReadDouble(buffer);
                else if (type == typeof(byte))
                    return (T)(object)ReadByte(buffer);
                else if (type == typeof(bool))
                    return (T)(object)ReadBool(buffer);
                else if (type == typeof(string))
                    return (T)(object)ReadString(buffer);
                else {
                    Logger.Log($"Tried to BinaryHelper.FromType bad type: {type.ToString()}");
                    Logger.Log(Environment.StackTrace.ToString());
                    return (T)(object)null;
                }
            }
        }

        /// <summary>
        /// Read a UInt16 from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static ushort ReadUInt16(BinaryReader buffer) {
            var val = buffer.ReadUInt16();
            if (Debug) Logger.Log($"ReadUInt16 {val} {val:X4}");
            return val;
        }

        /// <summary>
        /// Read an Int16 from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static short ReadInt16(BinaryReader buffer) {
            var val = buffer.ReadInt16();
            if (Debug) Logger.Log($"ReadInt16 {val} {val:X4}");
            return val;
        }

        /// <summary>
        /// Read a UInt32 from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static uint ReadUInt32(BinaryReader buffer) {
            var val = buffer.ReadUInt32();
            if (Debug) Logger.Log($"ReadUInt32 {val} {val:X8}");
            return val;
        }

        /// <summary>
        /// Read an Int32 from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static int ReadInt32(BinaryReader buffer) {
            var val = buffer.ReadInt32();
            if (Debug) Logger.Log($"ReadInt32 {val} {val:X8}");
            return val;
        }

        /// <summary>
        /// Read a UInt64 from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static ulong ReadUInt64(BinaryReader buffer) {
            var val = buffer.ReadUInt64();
            if (Debug) Logger.Log($"ReadUInt64 {val} {val:X16}");
            return val;
        }

        /// <summary>
        /// Read an Int64 from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static long ReadInt64(BinaryReader buffer) {
            var val = buffer.ReadInt64();
            if (Debug) Logger.Log($"ReadInt64 {val} {val:X16}");
            return val;
        }

        /// <summary>
        /// Read a Single(float) from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static float ReadSingle(BinaryReader buffer) {
            var val = buffer.ReadSingle();
            if (Debug) Logger.Log($"ReadSingle {val}");
            return val;
        }

        /// <summary>
        /// Read a double from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static double ReadDouble(BinaryReader buffer) {
            var val = buffer.ReadDouble();
            if (Debug) Logger.Log($"ReadDouble {val}");
            return val;
        }

        /// <summary>
        /// Read a bool from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static bool ReadBool(BinaryReader buffer) {
            var val = buffer.ReadInt32();
            if (Debug) Logger.Log($"ReadInt32 {val}");
            return val == 1;
        }

        /// <summary>
        /// Read a byte from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static byte ReadByte(BinaryReader buffer) {
            var val = buffer.ReadByte();
            if (Debug) Logger.Log($"ReadByte {val}");
            return val;
        }

        /// <summary>
        /// Read a PackedWORD (short) from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static short ReadPackedWORD(BinaryReader buffer) {
            short tmp = buffer.ReadByte();
            if ((tmp & 0x80) != 0)
                tmp = (short)(((tmp & 0x7f) << 8) | buffer.ReadByte());

            if (Debug) Logger.Log($"ReadPackedWORD {tmp}");
            return tmp;
        }

        /// <summary>
        /// Read a PackedDWORD (UInt32) from the buffer 
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static uint ReadPackedDWORD(BinaryReader buffer) {
            int tmp = buffer.ReadInt16();
            int otmp = tmp;
            if ((tmp & 0x8000) != 0) {
                tmp <<= 16;
                tmp &= 0x7FFFFFFF;
                tmp |= (ushort)buffer.ReadInt16();
            }
            return (uint)tmp;
        }

        /// <summary>
        /// Read an ascii string from the buffer. Automatically aligns to DWORD afterwards.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string ReadString(BinaryReader buffer) {
            long start = buffer.BaseStream.Position;
            int length = buffer.ReadInt16();
            if (length == -1) {
                length = buffer.ReadInt32();
            }

            byte[] tmp = buffer.ReadBytes(length);

            int align = (int)((buffer.BaseStream.Position - start) % 4);
            if (align > 0)
                buffer.ReadBytes(4 - align);

            var val = System.Text.Encoding.ASCII.GetString(tmp);

            if (Debug) Logger.Log($"ReadString <<{val}>>");
            return val;
        }

        /// <summary>
        /// Read a WString (unicode) from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string ReadWString(BinaryReader buffer) {
            int length = buffer.ReadByte();
            if ((length & 0x80) != 0) {
                length = ((length & 0x7f) << 8) | buffer.ReadByte();
            }

            byte[] tmp = buffer.ReadBytes(length * 2);

            var val = System.Text.Encoding.Unicode.GetString(tmp);

            if (Debug) Logger.Log($"ReadWString {val}");
            return val;
        }
    }
}
